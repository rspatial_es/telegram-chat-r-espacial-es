library(tidyverse)

load("rda/mensajes.rda")

mensajes %>% 
  count(date = lubridate::floor_date(date, "day"), 
        type_msg = str_extract(type_msg, "service|default") %>%
          str_replace_all(c("service" = "Sistema", "default" = "Usuarios"))) %>%
  ggplot(aes(date, n, color = type_msg)) +
  geom_line() +
  labs(y = "Msj. al dia", title = "Número de mensajes diarios en @Rspatial_es") +
  theme_minimal() +
  theme(legend.title = element_blank(),
        axis.title.x = element_blank(),
        plot.background = element_rect(fill = "#0c0c0c"),
        text = element_text(color = "grey90"),
        panel.grid = element_line(color = "grey20"))

ggsave("png/msj_dia.png", width = 6, height = 4, dpi = 150, scale = 1.75)

mensajes %>% 
  mutate(new_user = if_else(is.na(text), FALSE, str_detect(text, "invited")),
         total_users = 5 + cumsum(new_user)) %>%
  group_by(type_msg = str_extract(type_msg, "service|default"),
           date = lubridate::floor_date(date, "day")) %>%
  summarise(n = n(), 
            total_users = max(total_users)) %>%
  ggplot(aes(date, n, color = type_msg)) +
  geom_line() +
  geom_line(aes(y = total_users), color = "#0c0c0c")

mensajes %>%
  filter(!is.na(media_type)) %>%
  mutate(media_description = if_else(media_type == "file", "All documment files", 
                                     media_description)) %>%
  ggplot(aes(x = media_type, fill = media_description)) +
  geom_bar()+
  labs(y = "Conteo", title = "Contenido adjunto en los mensajes de @Rspatial_Es") +
  theme_minimal() +
  theme(legend.title = element_blank(),
        axis.title.x = element_blank(),
        plot.background = element_rect(fill = "#0c0c0c"),
        text = element_text(color = "grey90"),
        panel.grid = element_line(color = "grey20"))

ggsave("png/msj_adjunto.png", width = 6, height = 4, dpi = 150, scale = 1.75)

mensajes %>%
  filter(user != "Telegram Service") %>%
  count(user, sort = TRUE) %>%
  top_n(30) %>%
  inner_join(mensajes) %>%
  mutate(user = as_factor(user), 
         media_type = if_else(is.na(media_type), "no-attached", 
                              media_type)) %>%
  ggplot(aes(x = user, fill = media_type)) +
  geom_bar()+
  labs(y = "Conteo", title = "Top 10 usuarios de @Rspatial_Es",
       subtitle = "Todos los mensajes con o sin adjuntos") +
  theme_minimal() +
  theme(legend.title = element_blank(),
        axis.title.x = element_blank(),
        plot.background = element_rect(fill = "#0c0c0c"),
        text = element_text(color = "grey90"),
        panel.grid = element_line(color = "grey20"), 
        axis.text.x = element_text(angle = 90, hjust = 1))

ggsave("png/usr_top30.png", width = 6, height = 4, dpi = 150, scale = 1.75)

mensajes %>%
  filter(user != "Telegram Service") %>%
  count(user, sort = TRUE) %>%
  top_n(30) %>%
  inner_join(mensajes) %>%
  filter(!is.na(media_type)) %>%
  mutate(media_description = if_else(media_type == "file", "Documment", 
                                     media_description)) %>%
  mutate(user = as_factor(user)) %>%
  ggplot(aes(x = user, fill = media_description)) +
  geom_bar()+
  labs(y = "Conteo", title = "Top 10 usuarios @Rspatial_Es", 
       subtitle = "Solo mensajes con adjuntos") +
  theme_minimal() +
  theme(legend.title = element_blank(),
        axis.title.x = element_blank(),
        plot.background = element_rect(fill = "#0c0c0c"),
        text = element_text(color = "grey90"),
        panel.grid = element_line(color = "grey20"), 
        axis.text.x = element_text(angle = 90, hjust = 1))

ggsave("png/usr_top30_media.png", width = 6, height = 4, dpi = 150, scale = 1.75)
